#ifndef __GRAMMAR_H_
#define __GRAMMAR_H_

#include <stdio.h>

#include "config.h"
#include "parsing_context.h"
#include "grammar_tokens.h"
#include "grammar_semantics.h"
#include "token_node.h"
#define __GRAMMAR_SIZE 53


typedef struct gr_rule {
  uint32_t rule_number;
  gr_token lhs;
  uint8_t rhs_length;
  gr_token *rhs;
} gr_rule;

void init_grammar(parsing_ctx *ctx);
const char * gr_token_to_string(gr_token token);

#endif
