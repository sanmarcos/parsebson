#include "grammar.h"

const gr_token __gr_token_alloc[] = {file, document, elements, BINT32, END, INDEX, DOUBLE, UTF8_STRING, BINDATA, UNDEFINED, OBJECTID, FALSE, TRUE, UTC, NIL, REGEX, DBPOINTER, JSCODE, DEPRECATED, INT32, TIMESTAMP, INT64, MINKEY, MAXKEY, BYTE, EMBDOC, ARRAY, __TERM};

const char * const __gr_token_name[] = {"file", "document", "elements", "BINT32", "END", "INDEX", "DOUBLE", "UTF8_STRING", "BINDATA", "UNDEFINED", "OBJECTID", "FALSE", "TRUE", "UTC", "NIL", "REGEX", "DBPOINTER", "JSCODE", "DEPRECATED", "INT32", "TIMESTAMP", "INT64", "MINKEY", "MAXKEY", "BYTE", "EMBDOC", "ARRAY", "__TERM"};

const gr_rule __grammar[] = {
  {0, file, 1, (gr_token []){document}},
  {1, file, 3, (gr_token []){document, BINT32, END}},
  {2, file, 4, (gr_token []){document, BINT32, elements, END}},
  {3, document, 2, (gr_token []){BINT32, END}},
  {4, document, 3, (gr_token []){BINT32, elements, END}},
  {5, elements, 2, (gr_token []){elements, INDEX}},
  {6, elements, 2, (gr_token []){elements, DOUBLE}},
  {7, elements, 2, (gr_token []){elements, UTF8_STRING}},
  {8, elements, 2, (gr_token []){elements, BINDATA}},
  {9, elements, 2, (gr_token []){elements, UNDEFINED}},
  {10, elements, 2, (gr_token []){elements, OBJECTID}},
  {11, elements, 2, (gr_token []){elements, FALSE}},
  {12, elements, 2, (gr_token []){elements, TRUE}},
  {13, elements, 2, (gr_token []){elements, UTC}},
  {14, elements, 2, (gr_token []){elements, NIL}},
  {15, elements, 2, (gr_token []){elements, REGEX}},
  {16, elements, 2, (gr_token []){elements, DBPOINTER}},
  {17, elements, 2, (gr_token []){elements, JSCODE}},
  {18, elements, 2, (gr_token []){elements, DEPRECATED}},
  {19, elements, 2, (gr_token []){elements, INT32}},
  {20, elements, 2, (gr_token []){elements, TIMESTAMP}},
  {21, elements, 2, (gr_token []){elements, INT64}},
  {22, elements, 2, (gr_token []){elements, MINKEY}},
  {23, elements, 2, (gr_token []){elements, MAXKEY}},
  {24, elements, 2, (gr_token []){elements, BYTE}},
  {25, elements, 3, (gr_token []){elements, EMBDOC, END}},
  {26, elements, 4, (gr_token []){elements, EMBDOC, elements, END}},
  {27, elements, 3, (gr_token []){elements, ARRAY, END}},
  {28, elements, 4, (gr_token []){elements, ARRAY, elements, END}},
  {29, elements, 1, (gr_token []){INDEX}},
  {30, elements, 1, (gr_token []){DOUBLE}},
  {31, elements, 1, (gr_token []){UTF8_STRING}},
  {32, elements, 1, (gr_token []){BINDATA}},
  {33, elements, 1, (gr_token []){UNDEFINED}},
  {34, elements, 1, (gr_token []){OBJECTID}},
  {35, elements, 1, (gr_token []){FALSE}},
  {36, elements, 1, (gr_token []){TRUE}},
  {37, elements, 1, (gr_token []){UTC}},
  {38, elements, 1, (gr_token []){NIL}},
  {39, elements, 1, (gr_token []){REGEX}},
  {40, elements, 1, (gr_token []){DBPOINTER}},
  {41, elements, 1, (gr_token []){JSCODE}},
  {42, elements, 1, (gr_token []){DEPRECATED}},
  {43, elements, 1, (gr_token []){INT32}},
  {44, elements, 1, (gr_token []){TIMESTAMP}},
  {45, elements, 1, (gr_token []){INT64}},
  {46, elements, 1, (gr_token []){MINKEY}},
  {47, elements, 1, (gr_token []){MAXKEY}},
  {48, elements, 1, (gr_token []){BYTE}},
  {49, elements, 2, (gr_token []){EMBDOC, END}},
  {50, elements, 3, (gr_token []){EMBDOC, elements, END}},
  {51, elements, 2, (gr_token []){ARRAY, END}},
  {52, elements, 3, (gr_token []){ARRAY, elements, END}}
};
void init_grammar(parsing_ctx *ctx)
{
  ctx->gr_token_alloc = __gr_token_alloc;
  ctx->gr_token_name = __gr_token_name;
  ctx->grammar = __grammar;
}
const char * gr_token_to_string(gr_token token){
  if (token < __NTERM_LEN)
    return __gr_token_name[token]; //token is a nonterminal
  else if (is_terminal(token))
    return __gr_token_name[token_value(token)];  //token is a terminal
  else {
  fprintf(stdout, "ERROR> gr_token_to_string received a malformed input token: %d\n", token);
    exit(1);
  }
}