%nonterminal file
%nonterminal document
%nonterminal elements

%axiom file

%terminal INDEX
%terminal DOUBLE
%terminal UTF8_STRING
%terminal BINDATA
%terminal UNDEFINED
%terminal OBJECTID
%terminal FALSE
%terminal TRUE
%terminal UTC
%terminal NIL
%terminal REGEX
%terminal DBPOINTER
%terminal JSCODE
%terminal DEPRECATED
%terminal INT32
%terminal TIMESTAMP
%terminal INT64
%terminal MINKEY
%terminal MAXKEY
%terminal ARRAY 
%terminal EMBDOC
%terminal BINT32
%terminal END
%terminal BYTE


%%

file : document {  }
     | document BINT32 END {  }
     | document BINT32 elements END {  }
     ;

document : BINT32 END { }
         | BINT32 elements END { }
         ;

elements : elements INDEX { }
         | elements DOUBLE { }
         | elements UTF8_STRING { }
         | elements BINDATA { }
         | elements UNDEFINED { }
         | elements OBJECTID { }
         | elements FALSE { }
         | elements TRUE { }
         | elements UTC { }
         | elements NIL { }
         | elements REGEX { }
         | elements DBPOINTER { }
         | elements JSCODE { }
         | elements DEPRECATED { }
         | elements INT32 { }
         | elements TIMESTAMP { }
         | elements INT64 { }
         | elements MINKEY { }
         | elements MAXKEY { }
         | elements BYTE { } 
         | elements EMBDOC END { }
		 | elements EMBDOC elements END { }
         | elements ARRAY END { }
         | elements ARRAY elements END { }
         | INDEX { }
         | DOUBLE { }
         | UTF8_STRING { }
         | BINDATA { }
         | UNDEFINED { }
         | OBJECTID { }
         | FALSE { }
         | TRUE { }
         | UTC { }
         | NIL { }
         | REGEX { }
         | DBPOINTER { }
         | JSCODE { }
         | DEPRECATED { }
         | INT32 { }
         | TIMESTAMP { }
         | INT64 { }
         | MINKEY { }
         | MAXKEY { }
         | BYTE { }
         | EMBDOC END { }
		 | EMBDOC elements END { }
         | ARRAY END { }
         | ARRAY elements END { }
         ;
