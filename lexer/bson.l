%option noyywrap
%option nounput
%option noinput

%{
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "grammar_tokens.h"

struct lex_token {
    gr_token token;
    void* semantic_value;
};

extern struct lex_token* flex_token;

/* Function headers */
char *strclone(char *str);
char *getvarname(char *str);
long *getint(char *byte, int len);
double *getdouble(char *byte);

int doclevel;
%}

HEX_DIGIT [0-9a-fA-F]
HEX2 {HEX_DIGIT}{2}

BYTE (?s:.)
NONIL [^\x00]

BINT32 {BYTE}{4}
BINT64 {BYTE}{8}
NUM [0-9]


 //UNESCAPEDCHAR [ -!#-\[\]-~]
 //ESCAPEDCHAR \\["\\bfnrt/]
 //UNICODECHAR \\u{HEX_DIGIT}{HEX_DIGIT}{HEX_DIGIT}{HEX_DIGIT}
 //CHAR {UNESCAPEDCHAR}|{ESCAPEDCHAR}|{UNICODECHAR}

 //ASCII and UTF8 (N version does not match the newline)
 // https://stackoverflow.com/questions/9611682/flexlexer-support-for-unicode
ASC     [\x01-\x7f]
ASCN    [\x01-\t\v-\x7f]
U       [\x80-\xbf]
U2      [\xc2-\xdf]
U3      [\xe0-\xef]
U4      [\xf0-\xf4]

UANY    {ASC}|{U2}{U}|{U3}{U}{U}|{U4}{U}{U}{U}
UANYN   {ASCN}|{U2}{U}|{U3}{U}{U}|{U4}{U}{U}{U} 
UONLY   {U2}{U}|{U3}{U}{U}|{U4}{U}{U}{U}
 
CSTRING {UANY}*\x00
STRING {BINT32}{CSTRING}
INDEX {NUM}+\x00

DOUBLE \x01({CSTRING}|{INDEX}){BINT64}
UTF8_STRING \x02({CSTRING}|{INDEX}){STRING}
BINDATA \x05({CSTRING}|{INDEX})
UNDEFINED \x06({CSTRING}|{INDEX})
OBJECTID \x07({CSTRING}|{INDEX}){BYTE}{12}
FALSE \x08({CSTRING}|{INDEX})\x00
TRUE \x08({CSTRING}|{INDEX})\x01
UTC \x09({CSTRING}|{INDEX}){BINT64}
NIL \x0A({CSTRING}|{INDEX})
REGEX \x0B({CSTRING}|{INDEX})({CSTRING}|{INDEX}){2}
DBPOINTER \x0C({CSTRING}|{INDEX}){STRING}{BYTE}{12}
JSCODE \x0D({CSTRING}|{INDEX}){STRING}
DEPRECATED \x0E({CSTRING}|{INDEX}){STRING}
 //CODEWS {BINT32}{STRING}{DOCUMENT}
 //JSCODEWS \x0F({CSTRING}|{INDEX}){CODEWS}
INT32 \x10({CSTRING}|{INDEX}){BINT32}
TIMESTAMP \x11({CSTRING}|{INDEX}){BINT64}
INT64 \x12({CSTRING}|{INDEX}){BINT64}
MINKEY \xFF({CSTRING}|{INDEX})
MAXKEY \x7F({CSTRING}|{INDEX})
EMBDOC \x03({CSTRING}|{INDEX}){BINT32}
ARRAY \x04({CSTRING}|{INDEX}){BINT32}
END \x00
 //ELEMENT {DOUBLE}|{UTF8_STRING}|{BINDATA}|{UNDEFINED}|{OBJECTID}|{FALSE}|{TRUE}|{UTC}|{NIL}|{REGEX}|{DBPOINTER}|{JSCODE}|{DEPRECATED}|{CODEWS}|{JSCODEWS}|{INT32}|{TIMESTAMP}|{INT64}|{MINKEY}|{MAXKEY}
 //ELIST {ELEMENT}*
 //DOCUMENT {BINT32}{ELIST}\x00

%x inside

%%

<inside>{EMBDOC} { flex_token->token = EMBDOC;
                   doclevel++;
                   return 1;}
<inside>{ARRAY} { flex_token->token = ARRAY;
                  doclevel++;
                  return 1;}
<inside>{DOUBLE} { flex_token->token = DOUBLE;
                   flex_token->semantic_value = getdouble(&yytext[yyleng-8]);
                   return 1;}
<inside>{UTF8_STRING} { flex_token->token = UTF8_STRING;
                        return 1;}
<inside>{BINDATA} { flex_token->token = BINDATA;
                    flex_token->semantic_value = NULL;
                    return 1;}
<inside>{UNDEFINED} { flex_token->token = UNDEFINED;
                      flex_token->semantic_value = NULL;
                      return 1;}
<inside>{OBJECTID} { flex_token->token = OBJECTID;
                     return 1;}
<inside>{FALSE} { flex_token->token = FALSE;
                  return 1;}
<inside>{TRUE} { flex_token->token = TRUE;
                 return 1;}
<inside>{UTC} { flex_token->token = UTC;
                flex_token->semantic_value = getint(&yytext[yyleng-8], 8);
                return 1;}
<inside>{NIL} { flex_token->token = NIL;
                flex_token->semantic_value = NULL;
                return 1;}
<inside>{REGEX} { flex_token->token = REGEX;
                  flex_token->semantic_value = NULL;
                  return 1;}
<inside>{DBPOINTER} { flex_token->token = DBPOINTER;
                      flex_token->semantic_value = NULL;
                      return 1;}
<inside>{JSCODE} { flex_token->token = JSCODE;
                   flex_token->semantic_value = NULL;
                   return 1;}
<inside>{DEPRECATED} { flex_token->token = DEPRECATED;
                       flex_token->semantic_value = NULL;
                       return 1;}
<inside>{INT32} { flex_token->token = INT32;
                  flex_token->semantic_value = getint(&yytext[yyleng-4], 4);
                  return 1;}
<inside>{TIMESTAMP} { flex_token->token = TIMESTAMP;
                      flex_token->semantic_value = NULL;
                      return 1;}
<inside>{INT64} { flex_token->token = INT64;
                  flex_token->semantic_value = getint(&yytext[yyleng-8], 8);
                  return 1;}
<inside>{MINKEY} { flex_token->token = MINKEY;
                   flex_token->semantic_value = NULL;
                   return 1;}
<inside>{MAXKEY} { flex_token->token = MAXKEY;
                   flex_token->semantic_value = NULL;
                   return 1;}
<inside>{END} { flex_token->token = END;
                flex_token->semantic_value = NULL;
                doclevel--;
                if (doclevel==0)
                    BEGIN(INITIAL);
                return 1;}
<INITIAL>{BINT32} { flex_token->token = BINT32;
                    flex_token->semantic_value = NULL;
                    doclevel=1;
                    BEGIN(inside);
                    return 1;}
[ \t\n]+ printf("Found \\t or \\n\n");

<inside>{BYTE} { 
                    printf("Unexpected: %02X\n",*yytext);
}

%%

double *getdouble(char *byte){
    double *doub_to_ret = malloc (sizeof (double));
    memcpy(doub_to_ret, byte, 8);
    return doub_to_ret;
}

long *getint(char *byte, int len){
    long *long_to_ret = malloc (sizeof (long));
    memcpy(long_to_ret, byte, len);
    return long_to_ret;
}

char *getvarname(char *str) {
    char *varname;
    asprintf(&varname, str);
    return varname;
}

char *strclone(char *str)
{
    int len = strlen(str);
    char *clone = (char *)malloc(sizeof(char)*(len+1));
    strcpy(clone,str);
    return clone;
}

