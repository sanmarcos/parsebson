# EXAMPLE
execute_process(COMMAND ${SOURCEDIR}/bin/bson_parser "${SOURCEDIR}/tests/samples/example.bson"
                OUTPUT_FILE ${SOURCEDIR}/tests/output/example.txt
                )
execute_process(COMMAND sed "-i" "/Lexer/d" "${SOURCEDIR}/tests/output/example.txt"
                OUTPUT_QUIET
                )

execute_process(COMMAND sed "-i" "/Parser/d" "${SOURCEDIR}/tests/output/example.txt"
                OUTPUT_QUIET
                )


execute_process(COMMAND ${CMAKE_COMMAND} -E compare_files
    ${SOURCEDIR}/tests/output/example.txt ${SOURCEDIR}/tests/expected_output/example.txt
    RESULT_VARIABLE DIFFERENT)
if(DIFFERENT)
    message(FATAL_ERROR "Test failed - files differ")
endif()
