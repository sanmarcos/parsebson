# WIDGET
execute_process(COMMAND ${SOURCEDIR}/bin/bson_parser "${SOURCEDIR}/tests/samples/widget.bson"
                OUTPUT_FILE ${SOURCEDIR}/tests/output/widget.txt
                )
execute_process(COMMAND sed "-i" "/Lexer/d" "${SOURCEDIR}/tests/output/widget.txt"
                OUTPUT_QUIET
                )

execute_process(COMMAND sed "-i" "/Parser/d" "${SOURCEDIR}/tests/output/widget.txt"
                OUTPUT_QUIET
                )


execute_process(COMMAND ${CMAKE_COMMAND} -E compare_files
    ${SOURCEDIR}/tests/output/widget.txt ${SOURCEDIR}/tests/expected_output/widget.txt
    RESULT_VARIABLE DIFFERENT)
if(DIFFERENT)
    message(FATAL_ERROR "Test failed - files differ")
endif()
